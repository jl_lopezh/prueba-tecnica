# Jose Luis Lopez #

Prueba técnica para candidatura Java Backed Developer.

### Que contiene este repositorio ###

* Return Pair Numbers Exercise
* Lamda Factory Strategy Exercise
* Pyments Strategy Exercise

### Email ###

* [Jose Luis Lopez](joseluis.lopezhernandez@gmail.com)
