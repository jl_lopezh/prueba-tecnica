package interview;

public interface PaymentStrategy {
    double pay(double amount);

    static double CashPaymentStrategy(double amount) {
        double serviceCharge = 5.00;
        return amount + serviceCharge;
    }
    static double CreditCardStrategy(double amount) {
        double serviceCharge = 5.00;
        double creditCardFee = 10.00;
        return amount + serviceCharge + creditCardFee;
    }
}

class Main {

    private static String CASH = "CASH";
    private static String CREDIT_CARD = "CREDITCARD";

    public static PaymentStrategy getInstance(String tipo) {
        if (tipo.equals(CASH)) {
            return PaymentStrategy::CashPaymentStrategy;
        } else {
            return PaymentStrategy::CreditCardStrategy;
        }
    }

    public static void main(String[] args) {
        PaymentStrategy cashPayment = getInstance(CASH);
        PaymentStrategy ccPayment = getInstance(CREDIT_CARD);

        System.out.println(cashPayment.pay(100));
        System.out.println(ccPayment.pay(100));
    }
}
