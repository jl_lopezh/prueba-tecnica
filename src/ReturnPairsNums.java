package interview;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReturnPairsNums {

    public static List<Integer> returnPairs(List<Integer> list){
        Stream<Integer> pairs = list.stream().filter((num) -> (num % 2) == 0);
        return pairs.collect(Collectors.toList());
    }

    public static void main(String[] args){
        List<Integer> lista = Arrays.asList(1,2,3,4);
        List<Integer> result = returnPairs(lista);

        result.forEach(element -> System.out.println(element));
    }
}
