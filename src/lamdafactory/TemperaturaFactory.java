package interview.lamdafactory;

public class TemperaturaFactory {
    private static String INGLES = "INGLES";
    private static String INTERNACIONAL = "INTERNACIONAL";

    public static Temperatura getInstance(String tipo) {
        if (tipo.equals(INGLES)) {
            return Temperatura::ToFahrenheit;
        } else {
            return Temperatura::ToCelsius;
        }
    }

    public static void main(String[] args){
        Temperatura temFahrenheit = getInstance(INGLES);
        Temperatura tempCelsius = getInstance(INTERNACIONAL);

        System.out.println(temFahrenheit.calcular(32));
        System.out.println(tempCelsius.calcular(0));
    }
}

