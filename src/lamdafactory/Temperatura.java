package interview.lamdafactory;

public interface Temperatura {
    double calcular(double medicion);

    static double ToFahrenheit(double medicion) {
        return (medicion - 32) / 1.8;
    }
    static double ToCelsius(double medicion) {
        return (1.8 * medicion) + 32;
    }
}
